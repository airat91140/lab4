#include <fstream>
#include "graph.h"
using std::cout, std::endl, std::cin;

graph::graph() {
    root = new point[5]; //root
    mutextable = new std::mutex[5];
    root->marker = 2;
    number_m = 2;
    stop = false;
    srand(time(nullptr));

    root[0].out.push_back(new transport); // new 5 // root.out[0] = 5
    root[0].out.push_back(new transport); // new 6 // root.out[1] = 6
    root[0].out[0]->out.push_back(root + 1); // connect 5 - 1
    root[0].out[1]->out.push_back(root + 2); // connect 6 - 2
    root[0].out[0]->in.push_back(root); // connect root - 5
    root[0].out[1]->in.push_back(root); // connect root - 6

    root[1].out.push_back(new transport); // new 8 // 1.out[0] = 8
    root[1].out[0]->out.push_back(root + 3); // connect 8 - 3
    root[1].out[0]->out.push_back(root + 4); // connect 8 - 4
    root[1].out[0]->in.push_back(root + 1); // connect 1 - 8
    root[1].out[0]->in.push_back(root + 2); // connect 2 - 8

    root[2].out.push_back(root[1].out[0]); // 2.out[0] = 8
    root[2].out.push_back(new transport); // new 9 // 3.out = 5
    root[2].out[1]->out.push_back(root + 4); // connect 9 - 4
    root[2].out[1]->in.push_back(root + 2); // connect 2 - 9

    root[3].out.push_back(new transport); // new 7 // 3.out[0] = 7
    root[3].out[0]->out.push_back(root + 1); // connect 7 - 1
    root[3].out[0]->in.push_back(root + 3); // connect 3 - 7

    root[4].out.push_back(new transport); // new 11 // 4.out[0] = 8
    root[4].out.push_back(new transport); // new 10 // 4.out[0] = 8
    root[4].out[1]->out.push_back(root + 2); // connect 10 - 2
    root[4].out[0]->in.push_back(root + 4); // connect 4 - 11
    root[4].out[1]->in.push_back(root + 4); // connect 4 - 10
}

void graph::set_marker(int marker) {
    if (marker < 2)
        throw std::invalid_argument("Invalid marker");
    root->marker = marker;
    number_m = marker;
}

graph::~graph() {
    delete root[0].out[0];
    delete root[0].out[1];
    delete root[2].out[0];
    delete root[2].out[1];
    delete root[3].out[0];
    delete root[4].out[0];
    delete root[4].out[1];
    delete[] root;
    delete[] mutextable;
}

void graph::run_model() {
    std::thread t0([&](){point0();}), t1([&](){point1();}), t2([&](){point2();}), t3([&](){point3();}), t4([&](){point4();});
    while (!stop) {}
    t0.join();
    t1.join();
    t2.join();
    t3.join();
    t4.join();
    std::ofstream file("logs.txt", std::ios_base::out | std::ios_base::trunc);
    file << filelog.str();
}

std::stringstream graph::run_trans(transport *transport) {
    std::stringstream log;
    std::default_random_engine generator(time(nullptr));
    std::uniform_int_distribution<int> distribution(mintime, maxtime);
    std::this_thread::sleep_for(std::chrono::milliseconds(distribution(generator)));
    for (auto iter : transport->out) {
        logmu.lock();
        log << "Time " << time(nullptr) << ", marker to " << iter - root << " came, current marker: " << iter->marker + 1 << endl;
        logmu.unlock();
        ++iter->marker;
    }
    return log;
}

std::stringstream graph::calc(int m, int n) {
    std::stringstream log;
    mutextable[m].lock();
    --root[m].marker;
    if (root[m].marker < 0) {
        log << "Time " << time(nullptr) << ", marker from " << m << " to " << n << " blocked, current marker: " << root[m].marker + 1 << endl;
        ++root[m].marker;
        mutextable[m].unlock();
        return log;
    }
    mutextable[m].unlock();
    log << "Time " << time(nullptr) << ", marker from " << m << " gone to transport " << n << ", current marker: " << root[m].marker << endl;
    log << run_trans(root[m].out[n]).str();
    if (m == 4 && n == 0) {
        --number_m;
    }
    return log;
}

void graph::point0() {
    std::stringstream log;
    int tmp;
    while (!stop) {
        if (root[0].marker == 0)
            return;
        else {
            std::thread t1([&](){log << calc(0, tmp = rand() & 1).str();}), t2([&](){log << calc(0, 1 - tmp).str();});
            t1.join();
            t2.join();
        }
        logmu.lock();
        cout << log.str() << endl;
        filelog << log.str() << endl;
        logmu.unlock();
        log.str("");
    }
}

void graph::point1() {
    std::stringstream log;
    while (!stop) {
        if (root[1].marker == 0)
            continue;
        else if (root[1].marker == number_m)
            stop = true;
        else {
            if (root[1].out[0]->check_ins()) {
                mutextable[2].lock();
                mutextable[1].lock();
                --root[1].marker;
                --root[2].marker;
                if (root[2].marker < 0) {
                    ++root[2].marker;
                    ++root[1].marker;
                    log << "Time " << time(nullptr) << ", marker from 1 and 2 blocked, current marker on 1: " << root[1].marker << ", current marker on 2: " << root[2].marker << endl;
                    mutextable[1].unlock();
                    mutextable[2].unlock();
                    logmu.lock();
                    cout << log.str() << endl;
                    filelog << log.str() << endl;
                    logmu.unlock();
                    log.str("");
                    continue;
                }
                mutextable[1].unlock();
                mutextable[2].unlock();
                log << "Time " << time(nullptr) << ", marker from 1 and 2 gone, current marker on 1: " << root[1].marker << ", current marker on 2: " << root[2].marker << endl;
                log << run_trans(root[1].out[0]).str();
                logmu.lock();
                cout << log.str() << endl;
                filelog << log.str() << endl;
                logmu.unlock();
                log.str("");
            }
        }
    }
}

void graph::point2() {
    std::stringstream log;
    while (!stop) {
        if (root[2].marker == 0)
            continue;
        else {
            mutextable[2].lock();
            --root[2].marker;
            if (root[2].marker < 0) {
                ++root[2].marker;
                log << "Time " << time(nullptr) << ", marker from 2 to 4 blocked, current marker: " << root[2].marker << endl;
                mutextable[2].unlock();
                logmu.lock();
                cout << log.str() << endl;
                filelog << log.str() << endl;
                logmu.unlock();
                log.str("");
                continue;
            }
            log << "Time " << time(nullptr) << ", marker from 2 gone, current marker: " << root[2].marker << endl;
            mutextable[2].unlock();
            log << run_trans(root[2].out[1]).str();
            logmu.lock();
            cout << log.str() << endl;
            filelog << log.str() << endl;
            logmu.unlock();
            log.str("");
        }
    }
}

void graph::point3() {
    std::stringstream log;
    while (!stop) {
        if (root[3].marker == 0)
            continue;
        --root[3].marker;
        log << "Time " << time(nullptr) << ", marker from 3 gone, current marker: " << root[3].marker << endl;
        log << run_trans(root[3].out[0]).str();
        logmu.lock();
        cout << log.str() << endl;
        filelog << log.str() << endl;
        logmu.unlock();
        log.str("");
    }
}

void graph::point4() {
    std::stringstream log;
    int tmp;
    while (!stop) {
        if (root[4].marker == 0)
            continue;
        else {
            tmp = rand() & 1;
            std::thread t1([&](){log << calc(4, tmp).str();}), t2([&](){log << calc(4, 1 - tmp).str();});
            t1.join();
            t2.join();
            if (number_m == 1) {
                stop = true;
            }
        }
        logmu.lock();
        cout << log.str() << endl;
        filelog << log.str() << endl;
        logmu.unlock();
        log.str("");

    }
}

bool transport::check_ins() {
    if (std::all_of(in.begin(), in.end(), [&](point *i) { return i->marker > 0; }))
        return true;
    return false;
}

int get_num() {
    int number;
    while (true) {
        cin >> number;
        if (cin.good()) {
            return number;
        }
        cout << "You are wrong. Repeat, please." << endl;
        cin.clear();
        cin.ignore(32767, '\n');
    }
}