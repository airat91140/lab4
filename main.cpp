#include "graph.h"

int main() {
    graph gr;
    std::cout << "Enter number of markers" << std::endl;
    int er = 1;
    while (er) {
        try {
            gr.set_marker(get_num());
            er = 0;
        }
        catch (std::exception &error) {
            std::cout << "Error: " <<error.what() << std::endl;
        }
    }
    gr.run_model();
    return 0;
}
