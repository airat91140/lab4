#ifndef LAB4_GRAPH_H
#define LAB4_GRAPH_H
#include <vector>
#include <stdexcept>
#include <thread>
#include <random>
#include <map>
#include <mutex>
#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>

struct transport;

struct point {
    std::vector<transport *> out;
    int marker = 0;
};

struct transport {
    std::vector<point *> out, in;
    bool check_ins();
};

class graph {
private:
    std::stringstream filelog;
    bool stop;
    point *root;
    std::mutex *mutextable, logmu;
    int number_m;
    std::stringstream run_trans(transport *);
    void point0();
    std::stringstream calc(int, int);
    void point1();
    void point2();
    void point3();
    void point4();
    static const int mintime = 2000, maxtime = 5000;
public:
    graph();
    void set_marker(int);
    void run_model();
    ~graph();
};

int get_num();

#endif //LAB4_GRAPH_H
